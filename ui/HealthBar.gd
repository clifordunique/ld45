extends ColorRect

var actualValue:int = 20

func _ready() -> void:
	Events.connect("player_health_changed", self, "_onPlayerHealthChanged")

func _process(delta: float) -> void:
	visible = Unlocks.isUnlocked("health_bar")
	if Unlocks.isUnlocked("health"):
		$TextureProgress.value = actualValue
	else:
		$TextureProgress.value = min(2, actualValue)

func _onPlayerHealthChanged(health:int, maxHealth:int) -> void:
	$TextureProgress.max_value = maxHealth
	actualValue = health