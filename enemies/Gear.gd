extends Enemy

const SPEED:float = 18.0
var initialPos:Vector2
const MAX_DIST:float = 100.0
const ROTATE_SPEED:float = 2 * PI
const RESPAWN_TIME:float = 3.0
var initialHealth:int

func _ready() -> void:
	initialHealth = health
	initialPos = position
	Events.connect("screen_transitioned", self, "_onScreenTransitioned")
	$Sprite.visible = false
	yield(get_tree().create_timer(RESPAWN_TIME / 2), "timeout")
	$Sprite.visible = true

func die():
	if !$Sprite.visible:
		return
	var explosion = Explosion.instance()
	get_parent().add_child(explosion)
	explosion.global_position = global_position;
	explosion.emitting = true;
	$Sprite.visible = false
	position = initialPos
	health = initialHealth
	yield(get_tree().create_timer(RESPAWN_TIME), "timeout")
	$Sprite.visible = true

# Called when the node enters the scene tree for the first time.
func _doPhysicsProcess(delta: float):
	if Globals.screenTransitioning:
		return

	$CollisionShape2D.disabled = !$Sprite.visible

	if !$Sprite.visible:
		return

	for body in get_overlapping_bodies():
		var player:Player = body as Player
		if player != null:
			player.hit(damage)
	$Sprite.rotate(-ROTATE_SPEED * delta)

	var player:Player = Globals.player
	if player == null:
		return

	if global_position.distance_to(player.global_position) > MAX_DIST:
		return

	var dist:Vector2 = player.global_position - global_position
	position += dist.normalized() * SPEED * delta

func _onScreenTransitioned(screenName:String) -> void:
	position = initialPos
	health = initialHealth
	set_physics_process(Level.isInScreen(self, screenName))

