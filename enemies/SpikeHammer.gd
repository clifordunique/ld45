extends PathFollow2D

var sound:AudioStream = preload("spike-hammer.wav")
var filteredSound:AudioStream = preload("spike-hammer-filtered.wav")

func _ready() -> void:
	Events.connect("screen_transitioned", self, "_onScreenTransitioned")
	call_deferred("_setupTween")

func _setupTween():
	while true:
		yield(get_tree().create_timer(1), "timeout")
		while get_tree().paused:
			yield(get_tree(), "idle_frame")

		$Tween.interpolate_property(self, "unit_offset", 0, 0.999, .2, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		yield($Tween, "tween_completed")
		while get_tree().paused:
			yield(get_tree(), "idle_frame")

		Node2DShaker.shake(SceneManager.currentScene, .3, 1)
		if Unlocks.isUnlocked("game_hearing"):
			AudioPlayer.playSound(sound)
		else:
			AudioPlayer.playSound(filteredSound)
		yield(get_tree().create_timer(1), "timeout")
		while get_tree().paused:
			yield(get_tree(), "idle_frame")

		$Tween.interpolate_property(self, "unit_offset", 0.999, 0, 1.5, Tween.TRANS_LINEAR, Tween.EASE_IN)
		$Tween.start()
		yield($Tween, "tween_completed")
		while get_tree().paused:
			yield(get_tree(), "idle_frame")
		yield(get_tree().create_timer(1), "timeout")
		while get_tree().paused:
			yield(get_tree(), "idle_frame")

func _onScreenTransitioned(screenName:String) -> void:
	$Tween.playback_speed = 1 if Level.isInScreen(self, screenName) else 0
