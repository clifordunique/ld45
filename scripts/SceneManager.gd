extends ViewportContainer

var currentScene:Node;
var currentScenePacked:PackedScene;

func changeScene(scene:PackedScene) -> void:
	call_deferred("_actualChangeScene", scene)

func _actualChangeScene(scene:PackedScene) -> void:
	currentScenePacked = scene
	$Viewport.remove_child(currentScene)
	Globals.player = null
	currentScene.queue_free()
	currentScene = scene.instance()
	$Viewport.add_child(currentScene)
	currentScene.set_owner($Viewport)

func reloadScene() -> void:
	changeScene(currentScenePacked)

func _ready() -> void:
	call_deferred("_deferredReparentScene")

func _deferredReparentScene() -> void:
	var root:Viewport = get_tree().get_root()
	currentScene = root.get_child(root.get_child_count() - 1)
	root.remove_child(currentScene)
	$Viewport.add_child(currentScene)
	currentScene.set_owner($Viewport)
	currentScenePacked = load(currentScene.filename)
