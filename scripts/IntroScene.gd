extends CanvasLayer

const copyProtectScene:PackedScene = preload("res://scenes/copy-protect-scene.tscn")
const music:AudioStream = preload("res://music/intro.ogg")

func _ready() -> void:
	if SiteLockUrls.hasViolation():
		get_tree().change_scene_to(copyProtectScene)
		return

	randomize()
	ScreenTransitioner.instantOut()

	yield(get_tree().create_timer(0.5), "timeout")

	AudioPlayer.playSound(Sounds.sweepDown)
	ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS)
	yield(get_tree().create_timer(0.75), "timeout")
	AudioPlayer.playSound(music)
	yield(get_tree().create_timer(0.5), "timeout")

	yield(Yields.timeOrInputPressed(1.5, "ui_accept"), "completed")

	AudioPlayer.playSound(Sounds.sweepUp)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	var menuScene:PackedScene = load("res://scenes/menu-scene.tscn")
	SceneManager.changeScene(menuScene)
	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
