extends CanvasLayer

const menuChoice:PackedScene = preload("res://instances/menu-choice.tscn")
const music:AudioStream = preload("res://music/title.ogg")
const selectSound:AudioStream = preload("res://sfx/select.wav")
const confirmSound:AudioStream = preload("res://sfx/confirm.wav")
const lockedSound:AudioStream = preload("res://sfx/locked.wav")

const MAIN = [
	"Start Game",
	"Options",
	"Jukebox",
	"Credits",
	"Time Attack",
]

const OPTIONS = [
	"Unlock Menu Music",
	"Unlock Menu Graphic",
	"Unlock Menu Particles",
	"Unlock \"Start Game\"",
	"Reset Game Data",
	"Back",
]

const RESET = [
	"Confirm Reset",
	"Back",
]

const TIMEATTACK = [
	"Time Attack (Level 1 Start)",
	"Time Attack (Level 2 Start)",
	"Time Attack (Level 3 Start)",
	"Back",
]

const DUMMY = []

var selectedIndex:int = 0
var activeMenu:Array = DUMMY

func _ready() -> void:
	if Unlocks.isUnlocked("menu_music"):
		AudioPlayer.playMusic(music)
	if Unlocks.isUnlocked("main_menu"):
		activeMenu = MAIN
	if Globals.timeAttackMode:
		activeMenu = TIMEATTACK
	$BackgroundGraphic.visible = Unlocks.isUnlocked("menu_graphic")
	$Particles.visible = Unlocks.isUnlocked("menu_particles")

func _process(delta:float) -> void:
	$InitialText1.visible = activeMenu == DUMMY
	$InitialText2.visible = activeMenu == DUMMY
	$Menu.visible = activeMenu != DUMMY

	if activeMenu == DUMMY:
		if Input.is_action_just_pressed("ui_accept"):
			AudioPlayer.playSound(confirmSound)
			Unlocks.unlock("main_menu")
			activeMenu = MAIN
	else:
		_updateMenu()

	if Input.is_action_pressed("cheat_1") && Input.is_action_pressed("cheat_2") && Input.is_action_pressed("cheat_3"):
		if Input.is_action_just_pressed("cheat_time_attack") && !Unlocks.isUnlocked("time_attack"):
			Unlocks.unlock("time_attack")

func _updateMenu() -> void:
	var choices:int = activeMenu.size()
	while $Menu.get_child_count() < choices:
		$Menu.add_child(menuChoice.instance())
	if Input.is_action_just_pressed("ui_up"):
		AudioPlayer.playSound(selectSound)
		selectedIndex -= 1
		selectedIndex = (selectedIndex + choices) % choices
		while !$Menu.get_child(selectedIndex).visible:
			selectedIndex = (selectedIndex - 1 + choices) % choices
	if Input.is_action_just_pressed("ui_down"):
		AudioPlayer.playSound(selectSound)
		selectedIndex += 1
		selectedIndex = (selectedIndex + choices) % choices
		while !$Menu.get_child(selectedIndex).visible:
			selectedIndex = (selectedIndex + 1 + choices) % choices
	for i in $Menu.get_child_count():
		if i < choices:
			_updateChoice(i)
		else:
			$Menu.get_child(i).visible = false

	selectedIndex = (selectedIndex + choices) % choices

	while !$Menu.get_child(selectedIndex).visible:
		selectedIndex = (selectedIndex + 1 + choices) % choices

	if Input.is_action_just_pressed("ui_accept"):
		if $Menu.get_child(selectedIndex).locked:
			AudioPlayer.playSound(lockedSound)
			pass
		else:
			AudioPlayer.playSound(confirmSound)
			_executeChoice(activeMenu[selectedIndex])

func _updateChoice(index:int) -> void:
	var selected:bool = selectedIndex == index
	var unlocked:bool = true
	var visible:bool = true
	match activeMenu[index]:
		"Start Game":
			unlocked = Unlocks.isUnlocked("start_game")
		"Unlock Menu Music":
			visible = !Unlocks.isUnlocked("menu_music")
		"Unlock Menu Graphic":
			visible = !Unlocks.isUnlocked("menu_graphic")
		"Unlock Menu Particles":
			visible = !Unlocks.isUnlocked("menu_particles")
		"Unlock \"Start Game\"":
			visible = !Unlocks.isUnlocked("start_game")
		"Jukebox":
			unlocked = Unlocks.isUnlocked("jukebox")
		"Credits":
			unlocked = Unlocks.isUnlocked("credits")
		"Time Attack":
			unlocked = Unlocks.isUnlocked("time_attack")
	$Menu.get_child(index).updateChoice(visible, activeMenu[index], selected, !unlocked)

func _executeChoice(choice:String) -> void:
	match choice:
		"Options":
			activeMenu = OPTIONS
			selectedIndex = 0
		"Time Attack":
			activeMenu = TIMEATTACK
			selectedIndex = 0
		"Unlock Menu Music":
			set_process(false)
			#yield(Unlocks.unlock("menu_music"), "completed")
			Unlocks.unlock("menu_music")
			yield(get_tree().create_timer(1.75), "timeout")
			AudioPlayer.fadeMusic(0, 1, false)
			AudioPlayer.playMusic(music)
			set_process(true)
		"Unlock Menu Graphic":
			set_process(false)
			$BackgroundGraphic.rect_position = Vector2(100, 0)
			$BackgroundGraphic.visible = true
			$Tween.interpolate_property($BackgroundGraphic, "rect_position", Vector2(100, 0), Vector2(20, 0), 0.5, Tween.TRANS_CUBIC, Tween.EASE_OUT)
			$Tween.start()
			yield($Tween, "tween_completed")
			Unlocks.unlock("menu_graphic")
			set_process(true)
		"Unlock Menu Particles":
			$Particles.visible = true
			Unlocks.unlock("menu_particles")
		"Unlock \"Start Game\"":
			Unlocks.unlock("start_game")
		"Credits":
			set_process(false)
			AudioPlayer.fadeMusic(1)
			AudioPlayer.playSound(Sounds.sweepUp)
			yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
			yield(get_tree().create_timer(0.5), "timeout")
			var mainScene:PackedScene = load("res://scenes/credits.tscn")
			SceneManager.changeScene(mainScene)
			AudioPlayer.playSound(Sounds.sweepDown)
			yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
		"Jukebox":
			set_process(false)
			AudioPlayer.fadeMusic(1)
			AudioPlayer.playSound(Sounds.sweepUp)
			yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
			yield(get_tree().create_timer(0.5), "timeout")
			var mainScene:PackedScene = load("res://scenes/jukebox.tscn")
			SceneManager.changeScene(mainScene)
			AudioPlayer.playSound(Sounds.sweepDown)
			yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
		"Back":
			match activeMenu:
				OPTIONS:
					activeMenu = MAIN
					selectedIndex = 0
				TIMEATTACK:
					activeMenu = MAIN
					selectedIndex = 0
				RESET:
					activeMenu = OPTIONS
					selectedIndex = 0
		"Start Game":
			Globals.activeScreenName = ""
			Globals.igt1 = 0
			Globals.igt2 = 0
			Globals.igt3 = 0
			Globals.igtDisabled = false
			Globals.timeAttackMode = false
			Unlocks.resetForGame()
			set_process(false)
			AudioPlayer.fadeMusic(1)
			AudioPlayer.playSound(Sounds.sweepUp)
			yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
			yield(get_tree().create_timer(0.5), "timeout")
			var mainScene:PackedScene = load("res://scenes/light-cutscene.tscn")
			SceneManager.changeScene(mainScene)
			AudioPlayer.playSound(Sounds.sweepDown)
			yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
		"Time Attack (Level 1 Start)":
			TimeAttack(1)
		"Time Attack (Level 2 Start)":
			TimeAttack(2)
		"Time Attack (Level 3 Start)":
			TimeAttack(3)
		"Reset Game Data":
			activeMenu = RESET
			selectedIndex = 0
		"Confirm Reset":
			Globals.activeScreenName = ""
			Globals.igt1 = 0
			Globals.igt2 = 0
			Globals.igt3 = 0
			Globals.igtDisabled = false
			Globals.timeAttackMode = false
			Unlocks.resetAll()
			set_process(false)
			AudioPlayer.fadeMusic(1)
			AudioPlayer.playSound(Sounds.sweepUp)
			yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
			yield(get_tree().create_timer(0.5), "timeout")
			var mainScene:PackedScene = load("res://scenes/menu-scene.tscn")
			SceneManager.changeScene(mainScene)
			AudioPlayer.playSound(Sounds.sweepDown)
			yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")

func TimeAttack(level:int) -> void:
	Globals.activeScreenName = ""
	Globals.igt1 = 0
	Globals.igt2 = 0
	Globals.igt3 = 0
	Globals.igtDisabled = false
	Globals.timeAttackMode = true
	Unlocks.resetForTimeAttack(level)
	set_process(false)
	AudioPlayer.fadeMusic(1)
	AudioPlayer.playSound(Sounds.sweepUp)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(0.5), "timeout")
	var mainScene:PackedScene
	if level == 1:
		mainScene = load("res://levels/level1/level-1.tscn")
	elif level == 2:
		mainScene = load("res://levels/level2/level-2.tscn")
	else:
		mainScene = load("res://levels/level3/level-3.tscn")
	SceneManager.changeScene(mainScene)
	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
