extends Control

var locked:bool

func updateChoice(_visible:bool, text:String, selected:bool, _locked:bool, font:Font = null):
	locked = _locked
	visible = _visible
	$HBoxContainer/Label.text = text
	if _locked:
		$HBoxContainer/Label.add_color_override("font_color", Color(.6, .6, .6))
	else:
		$HBoxContainer/Label.add_color_override("font_color", Color(1, 1, 1))

	if font != null:
		$HBoxContainer/Label.add_font_override("font", font)

	$HBoxContainer/Label/Strike.visible = locked
	$HBoxContainer/Control2/Lock.visible = locked
	$HBoxContainer/Control/Arrow.visible = selected
