extends Node

const UNLOCKS:Dictionary = {
}

const AVAILABLE_UNLOCKS:Dictionary = {
	"main_menu": {
		"text": "The main menu",
		"description": "Your first unlock!",
	},
	"menu_music": {
		"text": "Main menu music",
		"description": "Jammin tunes!",
	},
	"menu_graphic": {
		"text": "Main menu graphic",
		"description": "Check out that shading!",
	},
	"menu_particles": {
		"text": "Main menu particles",
		"description": "Fancy...",
	},
	"start_game": {
		"text": "\"Start Game\"",
		"description": "Now you can actually start...",
	},
	"move_right": {
		"text": "Ability to move right",
		"description": "Press [Right] to move",
	},
	"move_left": {
		"text": "Ability to move left",
		"description": "Press [Left] to move",
	},
	"game_visuals": {
		"text": "Visual sensors",
		"description": "Gives you the ability to see the world!",
	},
	"health": {
		"text": "Health points",
		"description": "So you can survive some hits!\n(But watch out for spikes)",
	},
	"health_bar": {
		"text": "HP Bar",
		"description": "Shows your remaining health. You don't have much...",
	},
	"game_hearing": {
		"text": "Audio sensors",
		"description": "You can hear properly now!",
	},
	"jump": {
		"text": "Ability to jump",
		"description": "Press [Shift] or [X] to jump",
	},
	"game_music": {
		"text": "In-game music",
		"description": "It won't be so quiet anymore...",
	},
	"backgrounds": {
		"text": "Background art",
		"description": "You can see the world..."
	},
	"shoot": {
		"text": "Buster cannon",
		"description": "Press [Z] to fire!"
	},
	"multishot": {
		"text": "Buster ammo",
		"description": "Allows you fire more quickly!"
	},
	"level1_cutscene": {},
	"level2_cutscene": {},
	"level3_cutscene": {},
	"level2": {
		"text": "Stage #2",
		"description": "You made it out!",
	},
	"level3": {
		"text": "Stage #3",
		"description": "You've grown much stronger...",
	},
	"level2_key": {
		"text": "Key to the gate",
		"description": "But what lies beyond?",
	},
	"ending": {
		"text": "The ending",
		"description": "Well done.",
	},
	"boss": {
		"text": "The boss battle",
		"description": "Get ready...!",
	},
	"credits": {
		"text": "\"Credits\"",
		"description": "Thanks for playing till the end!",
	},
	"jukebox": {
		"text": "\"Jukebox\"",
		"description": "Listen to the entire soundtrack!",
	},
	"autofire": {
		"text": "\"Autofire\"",
		"description": "Hold down [Z] to fire!",
	},
	"recharge": {
		"text": "\"Health recharge\"",
		"description": "Your health has been restored!",
	},
	"level2_door": {
		"text": "The door",
		"description": "You unlocked the door!",
	},
	"time_attack": {
		"text": "Time attack mode",
		"description": "You've finished the game.\nNow do it faster!",
	},

	"metal_cutscene": {}
}

func resetForGame() -> void:
	UNLOCKS.erase("move_right")
	UNLOCKS.erase("move_left")
	UNLOCKS.erase("jump")
	UNLOCKS.erase("shoot")
	UNLOCKS.erase("multishot")
	UNLOCKS.erase("ending")
	UNLOCKS.erase("boss")
	UNLOCKS.erase("level1_cutscene")
	UNLOCKS.erase("level2_cutscene")
	UNLOCKS.erase("level3_cutscene")
	UNLOCKS.erase("metal_cutscene")
	UNLOCKS.erase("autofire")
	UNLOCKS.erase("recharge")
	UNLOCKS.erase("level2")
	UNLOCKS.erase("level2_key")
	UNLOCKS.erase("level2_door")
	UNLOCKS.erase("level3")
	UNLOCKS.erase("backgrounds")
	UNLOCKS.erase("game_music")
	UNLOCKS.erase("game_hearing")
	UNLOCKS.erase("game_visuals")
	UNLOCKS.erase("health")
	UNLOCKS.erase("health_bar")
	pass

func resetForTimeAttack(level:int) -> void:
	resetForGame()
	var i:int = 1
	while i <= 3 && i <= level:
		unlocksForLevel(i)
		i = i + 1

func unlocksForLevel(level:int) -> void:
	match level:
		1:
			pass
		2:
			unlock("move_right", false)
			unlock("move_left", false)
			unlock("game_hearing", false)
			unlock("level1_cutscene", false)
			unlock("jump", false)
			unlock("level2", false)
			unlock("game_visuals", false)
			unlock("game_music", false)
			unlock("health", false)
			unlock("health_bar", false)
		3:
			unlock("level2_cutscene", false)
			unlock("shoot", false)
			unlock("multishot", false)
			unlock("level3", false)
			unlock("level2_key", false)
			unlock("level2_door", false)
			unlock("backgrounds", false)

func _ready() -> void:
	var file = File.new()
	if file.open("user://savegame.txt", File.READ) == OK:
		var temp = parse_json(file.get_as_text())
		for key in temp:
			UNLOCKS[key] = temp[key]
		file.close()

	if OS.is_debug_build():
		unlock("move_right", false)
		unlock("move_left", false)
		unlock("game_hearing", false)
		unlock("level1_cutscene", false)
		unlock("level2_cutscene", false)
		unlock("level2_key", false)
		unlock("jump", false)
		unlock("game_music", false)
		unlock("health", false)
		unlock("health_bar", false)
		unlock("backgrounds", false)
		unlock("shoot", false)
		unlock("multishot", false)
		unlock("autofire", false)
		#unlock("level3_cutscene", false)
		#unlock("boss", false)
		#unlock("metal_cutscene", false)
		#unlock("credits", false)
		#unlock("jukebox", false)
		#for feature in AVAILABLE_UNLOCKS:
			#unlock(feature, false)
		#for feature in AVAILABLE_UNLOCKS:
			#UNLOCKS.erase(feature)

func resetAll() -> void:
	for feature in AVAILABLE_UNLOCKS:
		UNLOCKS.erase(feature)
	var save_game = File.new()
	save_game.open("user://savegame.txt", File.WRITE)
	save_game.store_line(to_json(UNLOCKS))
	save_game.close()

func unlock(feature:String, usePopup:bool = true) -> void:
	UNLOCKS[feature] = OS.get_unix_time()
	var definition:Dictionary = AVAILABLE_UNLOCKS[feature]

	# HACKIITY
	if feature == "recharge" && Globals.player != null:
		Globals.player.recharge()
		yield(get_tree(), "idle_frame")
		yield(get_tree(), "idle_frame")

	if usePopup:
		yield(UnlockPopup.show(get_tree(), definition), "completed")

	var save_game = File.new()
	save_game.open("user://savegame.txt", File.WRITE)
	save_game.store_line(to_json(UNLOCKS))
	save_game.close()

func isUnlocked(feature:String) -> bool:
	return UNLOCKS.has(feature)