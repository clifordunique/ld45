extends Level

var filteredSound:AudioStream = preload("res://enemies/spike-hammer-filtered.wav")
var libraImage:Texture = preload("res://dialog-box/libra.png")

func _ready() -> void:
	if Unlocks.isUnlocked("level1_cutscene"):
		return

	if Globals.timeAttackMode || true:
		Globals.igtDisabled = true
		Globals.conveyorsDisabled = false
		$CanvasLayer/BlackRect.visible = false
		Unlocks.unlock("game_visuals", false)
		get_node("Screens/1-1/Frozen").visible = false
		Unlocks.unlock("level1_cutscene", false)
		#$Player.global_position = $Screens.get_node("1-4").global_position
		$Player.position = Vector2(207.999, 312.874)
		Globals.activeScreenName = "1-4"
		Events.emit_signal("screen_transitioned", Globals.activeScreenName)
		var collisionShape:CollisionShape2D = _activeScreen().get_node("CollisionShape2D");
		var camera:Camera2D = $Player.get_node("Camera2D")
		camera.limit_left = collisionShape.global_position.x - collisionShape.shape.extents.x
		camera.limit_right = collisionShape.global_position.x + collisionShape.shape.extents.x
		camera.limit_top = collisionShape.global_position.y - collisionShape.shape.extents.y
		camera.limit_bottom = collisionShape.global_position.y + collisionShape.shape.extents.y
	
		while !Unlocks.isUnlocked("move_right"):
			yield(get_tree(), "idle_frame")
		Globals.igtDisabled = false
		return

	Globals.conveyorsDisabled = true
	$CanvasLayer/BlackRect.visible = true
	yield(get_tree().create_timer(2), "timeout")
	yield(DialogBox.show(get_tree(), ["---REBOOT COMPLETE---", "---VISUAL SYSTEMS NOMINAL---"], null, 0.5), "completed")
	$Tween.interpolate_property(
		$CanvasLayer/BlackRect,
		"modulate",
		Color(1, 1, 1, 1),
		Color(1, 1, 1, 0),
		3,
		Tween.TRANS_LINEAR,
		Tween.EASE_IN
	)
	$Tween.start()
	yield($Tween, "tween_completed")
	AudioPlayer.playSound(Sounds.confirm)
	yield(Unlocks.unlock("game_visuals"), "completed")
	$CanvasLayer/BlackRect.visible = false
	yield(get_tree().create_timer(2), "timeout")
	yield(DialogBox.show(get_tree(), [
		"Libra: ...",
		"Libra: Where...am...I?",
		"Libra: Can't remember...",
		"Libra: ...!",
		"Libra: My mobility subroutine is\noffline...",
		"Libra: I...can't move...",
	], libraImage, 1.5), "completed")
	yield(get_tree().create_timer(2), "timeout")
	Node2DShaker.shake(SceneManager.currentScene, .3, 1)
	AudioPlayer.playSound(filteredSound, 1.0)
	yield(get_tree().create_timer(1), "timeout")
	yield(DialogBox.show(get_tree(), [
		"Libra: ...!?",
	], libraImage, 1.5), "completed")
	yield(get_tree().create_timer(1), "timeout")
	Globals.conveyorsDisabled = false
	get_node("Screens/1-1/Frozen").visible = false

	while Globals.activeScreenName != "1-4":
		yield(get_tree(), "idle_frame")

	yield(get_tree().create_timer(2), "timeout")
	yield(DialogBox.show(get_tree(), [
		"Libra: ...that subroutine!",
		"Libra: It looks like it's\ncompatible with my system...",
	], libraImage, 1.5), "completed")

	while !Unlocks.isUnlocked("move_right"):
		yield(get_tree(), "idle_frame")

	while get_tree().paused:
		yield(get_tree(), "idle_frame")

	yield(get_tree().create_timer(1), "timeout")

	yield(DialogBox.show(get_tree(), [
		"Libra: Well...that's a start.",
		"Libra: But I'm still barely\nfunctional...",
	], libraImage, 1.5), "completed")

	Unlocks.unlock("level1_cutscene", false)

func _physics_process(delta:float) -> void:
	if !Globals.igtDisabled && !Globals.screenTransitioning:
		Globals.igt1 += 1