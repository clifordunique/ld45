extends Area2D

var active:bool = true

func _ready() -> void:
	connect("body_entered", self, "_onBodyEntered")

func _onBodyEntered(body:PhysicsBody2D) -> void:
	if !body is Player:
		return

	if !active:
		return

	active = false

	Globals.igtDisabled = true

	AudioPlayer.playSound(Sounds.sweepUp)
	AudioPlayer.fadeMusic(1)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(1), "timeout")


	if Globals.timeAttackMode:
		AudioPlayer.playSound(Sounds.confirm)
		Unlocks.unlock("level2", false)
		yield(UnlockPopup.showTimeAttack(get_tree(), 1), "completed")
	else:
		AudioPlayer.playSound(Sounds.confirm)
		yield(Unlocks.unlock("level2"), "completed")
	yield(get_tree().create_timer(1), "timeout")
	var level2:PackedScene = load("res://levels/level2/level-2.tscn")
	Globals.activeScreenName = ""
	SceneManager.changeScene(level2)
	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
