extends Area2D

var libraImage:Texture = preload("res://dialog-box/libra.png")
var active:bool = true

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	connect("body_entered", self, "_onBodyEntered")

func _onBodyEntered(body:PhysicsBody2D) -> void:
	if !body is Player:
		return
	if !active:
		return

	active = false

	Globals.igtDisabled = true

	yield(DialogBox.show(get_tree(), [
		"Libra: I still don't know where\nI am...",
		"Libra: But whatever's behind\nthis door might help me find out.",
		"Libra: ...here goes nothing.",
	], libraImage, 1.5), "completed")

	AudioPlayer.playSound(Sounds.sweepUp)
	AudioPlayer.fadeMusic(1)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(1), "timeout")

	if Globals.timeAttackMode:
		AudioPlayer.playSound(Sounds.confirm)
		Unlocks.unlock("level3", false)
		yield(UnlockPopup.showTimeAttack(get_tree(), 2), "completed")
	else:
		AudioPlayer.playSound(Sounds.confirm)
		yield(Unlocks.unlock("level3"), "completed")
	yield(get_tree().create_timer(1), "timeout")
	var level3:PackedScene = load("res://levels/level3/level-3.tscn")
	Globals.activeScreenName = ""
	SceneManager.changeScene(level3)
	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
