extends ParallaxLayer

# Declare member variables here. Examples:
# var a: int = 2
# var b: String = "text"

func _ready() -> void:
	pause_mode = Node.PAUSE_MODE_PROCESS

func _process(delta: float) -> void:
	visible = !Unlocks.isUnlocked("backgrounds")
