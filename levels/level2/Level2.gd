extends Level

var libraImage:Texture = preload("res://dialog-box/libra.png")

func _ready() -> void:
	_subCutscene()
	if Unlocks.isUnlocked("level2_cutscene"):
		return

	yield(get_tree().create_timer(2), "timeout")
	yield(DialogBox.show(get_tree(), [
		"Libra: I'm outside...",
		"Libra: I can run and jump now,\nbut...",
		"Libra: I feel defenseless\nwithout a weapon...",
	], libraImage, 1.5), "completed")

	Unlocks.unlock("level2_cutscene", false)


func _process(delta: float) -> void:
	$ParallaxBackground.offset.y = 0
	$ParallaxBackground/ParallaxLayer.position.y = 0
	$ParallaxBackground/ParallaxLayer2.position.y = 0
	for child in $ParallaxBackground/ParallaxLayer.get_children():
		child.position.y = 0
	for child in $ParallaxBackground/ParallaxLayer2.get_children():
		child.position.y = 0
	$ParallaxBackground/HidingRect.position.y = 0
	$ParallaxBackground/HidingRect/ColorRect.rect_position.y = 0

func _subCutscene() -> void:
	if Unlocks.isUnlocked("shoot"):
		return

	while !Unlocks.isUnlocked("shoot"):
		yield(get_tree(), "idle_frame")
	while get_tree().paused:
		yield(get_tree(), "idle_frame")

	yield(DialogBox.show(get_tree(), [
		"Libra: This weapon...",
		"Libra: I've seen it before\nsomewhere...",
		"Libra: I...can't remember...",
	], libraImage, 1.5), "completed")


func _physics_process(delta:float) -> void:
	if !Globals.igtDisabled && !Globals.screenTransitioning:
		Globals.igt2 += 1