extends Level

var libraImage:Texture = preload("res://dialog-box/libra.png")

func _ready() -> void:
	_subCutscene()
	if Unlocks.isUnlocked("level3_cutscene"):
		return

	yield(get_tree().create_timer(4), "timeout")
	yield(DialogBox.show(get_tree(), [
		"Libra: This place...",
		"Libra: It's familiar...",
	], libraImage, 1.5), "completed")

	Unlocks.unlock("level3_cutscene", false)


func _process(delta: float) -> void:
	$ParallaxBackground.offset.y = 0
	$ParallaxBackground/ParallaxLayer.position.y = 0
	$ParallaxBackground/ParallaxLayer2.position.y = 0
	$ParallaxBackground/ParallaxLayer3.position.y = 0
	$ParallaxBackground/ParallaxLayer4.position.y = 0
	for child in $ParallaxBackground/ParallaxLayer.get_children():
		child.position.y = 0
	for child in $ParallaxBackground/ParallaxLayer2.get_children():
		child.position.y = 0
	for child in $ParallaxBackground/ParallaxLayer3.get_children():
		child.position.y = 0
	for child in $ParallaxBackground/ParallaxLayer4.get_children():
		child.position.y = 0
	$ParallaxBackground/HidingRect.position.y = 0
	$ParallaxBackground/HidingRect/ColorRect.rect_position.y = 0

func _subCutscene() -> void:
	if Unlocks.isUnlocked("shoot"):
		return

	while !Unlocks.isUnlocked("shoot"):
		yield(get_tree(), "idle_frame")
	while get_tree().paused:
		yield(get_tree(), "idle_frame")

	yield(DialogBox.show(get_tree(), [
		"Libra: This weapon...",
		"Libra: I've seen it before somewhere...",
		"Libra: I...can't remember...",
	], libraImage, 1.5), "completed")

func _physics_process(delta:float) -> void:
	if !Globals.igtDisabled && !Globals.screenTransitioning:
		Globals.igt3 += 1