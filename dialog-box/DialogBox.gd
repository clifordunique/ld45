extends CanvasLayer

class_name DialogBox

const DELAY:float = 0.02

const scene:PackedScene = preload("dialog-box.tscn")
const topScene:PackedScene = preload("dialog-box-top.tscn")

static func show(sceneTree:SceneTree, dialogs:Array, picture:Texture, pitch:float, top:bool = false, fade:bool = true) -> void:
	if Globals.timeAttackMode:
		yield(sceneTree, "idle_frame")
		return
	if fade:
		AudioPlayer.fadeMusic(0.25, 0.65, false)
	sceneTree.paused = true
	var instance:CanvasLayer
	if top:
		instance = topScene.instance()
	else:
		instance = scene.instance()
	instance.pause_mode = Node.PAUSE_MODE_PROCESS
	sceneTree.get_root().add_child(instance)
	instance.get_node("NinePatchRect/Text").visible = false
	instance.get_node("NinePatchRect/TextureRect").visible = false
	instance.get_node("NinePatchRect/TextureRect").texture = picture
	var tween = instance.get_node("Tween")
	var container = instance.get_node("NinePatchRect")
	container.rect_scale = Vector2(0, 0)
	tween.interpolate_property(container, "rect_scale", Vector2(0, 0), Vector2(1, 1), .25, Tween.TRANS_QUAD, Tween.EASE_OUT)
	tween.start()
	yield(tween, "tween_completed")

	instance.get_node("NinePatchRect/Text").visible = true
	instance.get_node("NinePatchRect/TextureRect").visible = true
	instance.get_node("Sound").pitch_scale = pitch
	for dialog in dialogs:
		yield(_showDialog(sceneTree, instance, dialog), "completed")

	tween.interpolate_property(container, "rect_scale", Vector2(1, 1), Vector2(0, 0), .25, Tween.TRANS_QUAD, Tween.EASE_IN)
	tween.start()
	yield(tween, "tween_completed")
	instance.queue_free()
	sceneTree.paused = false
	if fade:
		AudioPlayer.fadeMusic(.4, 1, false)

static func _showDialog(sceneTree:SceneTree, instance:CanvasLayer, text:String) -> void:
	if OS.is_debug_build() && Input.is_action_pressed("shoot"):
		yield(sceneTree, "idle_frame")
		return

	for i in range(text.length() + 1):
		if i % 2 == 0:
			instance.get_node("Sound").play()
		var first:String = text.substr(0, i)
		var second:String = text.substr(i, text.length() - i)
		instance.get_node("NinePatchRect/Text").bbcode_text = first + "[color=#ff000000]" + second + "[/color]"
		yield(sceneTree.create_timer(DELAY), "timeout")

	yield(Yields.inputPressed("ui_accept"), "completed")
