extends Node

const light:Texture = preload("res://dialog-box/dr-light.png")
const libra:Texture = preload("res://dialog-box/libra.png")
const music:AudioStream = preload("res://music/credits.ogg")

func _ready() -> void:
	yield(get_tree().create_timer(1), "timeout")
	yield(DialogBox.show(get_tree(), [
		"???: ...Libra!",
	], light, 1), "completed")
	yield(DialogBox.show(get_tree(), [
		"Libra: ...",
		"Libra: ...who...are you...?",
	], libra, 1.5), "completed")
	yield(DialogBox.show(get_tree(), [
		"???: ...!!",
		"???: Your memory circuits...",
		"???: I guess you don't remember\nanything, then.",
		"???: ...so much has happened...",
	], light, 1), "completed")
	yield(DialogBox.show(get_tree(), [
		"Libra: ...are you...the one who\ncreated me?",
	], libra, 1.5), "completed")
	yield(DialogBox.show(get_tree(), [
		"???: ...",
		"???: I'm just a fool...who once believed in a dream.",
		"???: ...in the next room you'll\nfind a memory card.",
		"???: Download the data and\nyou'll understand everything.",
		"???: Just...make sure you're\nready.",
	], light, 1), "completed")
	yield(DialogBox.show(get_tree(), [
		"Libra: ......",
	], libra, 1.5), "completed")

	yield(get_tree().create_timer(2), "timeout")
	AudioPlayer.playSound(Sounds.sweepUp)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(2), "timeout")


	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")

	AudioPlayer.playMusic(music)
	yield(DialogBox.show(get_tree(), [
		"Who is this disillusioned man?",
		"What will Libra find on that\nmemory card?",
		"And why does she have such a bad feeling about it?",
		"The only thing that she knows\nfor sure...",
		"...is that whatever happens, she can rely on the buster cannon on her arm.",
		"...TO BE CONTINUED...",
	], null, 1.0, false, false), "completed")
	AudioPlayer.fadeMusic(2)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(2), "timeout")

	yield(Unlocks.unlock("credits"), "completed")
	yield(Unlocks.unlock("jukebox"), "completed")
	yield(Unlocks.unlock("time_attack"), "completed")


	var mainScene:PackedScene = load("res://scenes/menu-scene.tscn")
	SceneManager.changeScene(mainScene)
	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")


