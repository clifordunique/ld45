extends Node

const light:Texture = preload("res://dialog-box/dr-light.png")

func _ready() -> void:
	yield(get_tree().create_timer(1), "timeout")
	yield(DialogBox.show(get_tree(), [
		"???: Libra...",
		"???: Libra...can you hear me...?",
		"???: ...",
		"???: It looks like her\naudiovisual sensors were damaged in the explosion...",
		"???: ...I can fix this...",
		"???: ......",
		"???: ...alright.",
		"???: The visual subsystem should work after this reboot finishes--",
		"???: --No! ",
		"???: ...damn...there's no time...",
		"???: ...",
		"???: ...be safe, Libra.",
	], light, 1), "completed")
	AudioPlayer.playSound(Sounds.sweepUp)
	yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
	yield(get_tree().create_timer(2), "timeout")
	var mainScene:PackedScene = load("res://levels/level1/level-1.tscn")
	SceneManager.changeScene(mainScene)
	AudioPlayer.playSound(Sounds.sweepDown)
	yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
