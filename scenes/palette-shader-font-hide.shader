shader_type canvas_item;

uniform float threshold1 = 0.25;
uniform float threshold2 = 0.5;
uniform float threshold3 = 0.75;

uniform vec4 color1 : hint_color = vec4(0, 0, 0, 1);
uniform vec4 color2 : hint_color = vec4(0.33, 0.33, 0.33, 1);
uniform vec4 color3 : hint_color = vec4(0.66, 0.66, 0.66, 1);
uniform vec4 color4 : hint_color = vec4(1, 1, 1, 1);

float lum(vec3 color) {
	return (0.2126*color.r + 0.7152*color.g + 0.0722*color.b);
}

void fragment(){
	if (COLOR.r < 0.01 && COLOR.g < 0.01 && COLOR.b < 0.01) {
		discard;
	}
	COLOR = texture(TEXTURE, UV); //read from texture
	float lum0 = lum(COLOR.rgb);
	
	if (lum0 < 0.01) {
		discard;
	}
	
	float lum1 = lum(color1.rgb);
	float lum2 = lum(color2.rgb);
	float lum3 = lum(color3.rgb);
	float lum4 = lum(color4.rgb);

	if (lum0 < threshold1) COLOR.rgb = color1.rgb;
	else if (lum0 < threshold2) COLOR.rgb = color2.rgb;
	else if (lum0 < threshold3) COLOR.rgb = color3.rgb;
	else COLOR.rgb = color4.rgb;
}

