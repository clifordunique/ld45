extends CanvasLayer

const menuChoice:PackedScene = preload("res://instances/menu-choice.tscn")
const selectSound:AudioStream = preload("res://sfx/select.wav")
const confirmSound:AudioStream = preload("res://sfx/confirm.wav")
const lockedSound:AudioStream = preload("res://sfx/locked.wav")

const normalFont:Font = preload("res://fonts/unnamed-3x5.tres")
const playingFont:Font = preload("res://fonts/unnamed-3x5-outlined.tres")

const MAIN = [
	"Intro",
	"Unlock",
	"Title",
	"Level 1",
	"Level 2",
	"Level 3",
	"Boss",
	"Credits",
	"(Back)",
]

const DESCRIPTIONS = {
	"Intro": "Whistle in the Wind (Intro)\nWritten in 7 mins\n\nA short intro melody. For these I usually try to riff off the \"main melody\" of the game, but I didn't end up doing that here - it's just a simple 5 note riff.",
	"Unlock": "You Did It! (Unlock)\nWritten in 14 mins\n\nThis is actually the second version of the unlock ditty I wrote - the first one was way too bright and cheery. This one is in minor instead and references the \"Boss Selected\" melody from Mega Man.",
	"Title": "Unlock Everything (Title)\nWritten in 41 mins\n\nGood old Mega Man sounds! Although this soundtrack is not 100% authentic NES chiptune sound, I arranged it in adherence to the traditional limitations and stylings of the NES soundchip.",
	"Level 1": "Searching for Answers (Level 1)\nWritten in 23 mins\n\nThis track is a little more somber in mood to match the feeling of Libra being lost without any of her abilities. It's also got the lowest tempo of the 3 stage themes to match the gameplay.",
	"Level 2": "Face Your Destiny (Level 2)\nWritten in 74 mins\n\nThis is my favorite track by far of the entire soundtrack. I spent the most time on it, but every minute was worth it. It's upbeat and got really nice melodies and fills. I could listen to this on repeat...",
	"Level 3": "Metal Mania (Level 3)\n[Mega Man 2 - Metal Man]\nWritten in 32 mins\n\nI wrote this remix very early on after a flash of inspiration. I was really excited about it and it ended up dictating the entire stage 3 and boss designs.",
	"Boss": "Deadly Dance (Boss)\nWritten in 21 mins\n\nPretty standard boss battle stuff - I knew this song didn't have to be particularly long so I didn't spent a huge amount of time on it. The melody uses a pretty common echoing effect utilizing both pulse channels.",
	"Credits": "To Be Continued (Credits)\n[Mega Man 2 - Pass Word]\nWritten in 9 mins\n\nWe close things off with a really quick rendition of the password theme from Mega Man, which I thought would be fitting for the ending.\nThanks for listening :)",
}

var selectedIndex:int = 0
var activeMenu:Array = MAIN
var nowPlaying:String = ""

func _process(delta:float) -> void:
	_updateMenu()
	$NowPlaying.text = "Now playing: " + nowPlaying

func _updateMenu() -> void:
	var choices:int = activeMenu.size()
	while $Menu.get_child_count() < choices:
		$Menu.add_child(menuChoice.instance())
	if Input.is_action_just_pressed("ui_up"):
		AudioPlayer.playSound(selectSound)
		selectedIndex -= 1
		selectedIndex = (selectedIndex + choices) % choices
		while !$Menu.get_child(selectedIndex).visible:
			selectedIndex = (selectedIndex - 1 + choices) % choices
	if Input.is_action_just_pressed("ui_down"):
		AudioPlayer.playSound(selectSound)
		selectedIndex += 1
		selectedIndex = (selectedIndex + choices) % choices
		while !$Menu.get_child(selectedIndex).visible:
			selectedIndex = (selectedIndex + 1 + choices) % choices
	for i in $Menu.get_child_count():
		if i < choices:
			_updateChoice(i)
		else:
			$Menu.get_child(i).visible = false

	selectedIndex = (selectedIndex + choices) % choices

	while !$Menu.get_child(selectedIndex).visible:
		selectedIndex = (selectedIndex + 1 + choices) % choices

	$Description.text = ""
	if DESCRIPTIONS.has(activeMenu[selectedIndex]):
		$Description.text = DESCRIPTIONS[activeMenu[selectedIndex]];

	if Input.is_action_just_pressed("ui_accept"):
		if $Menu.get_child(selectedIndex).locked:
			pass
		else:
			_executeChoice(activeMenu[selectedIndex])

func _updateChoice(index:int) -> void:
	var selected:bool = selectedIndex == index
	var unlocked:bool = true
	var visible:bool = true
	var font:Font = normalFont
	if nowPlaying == activeMenu[index]:
		font = playingFont
	$Menu.get_child(index).updateChoice(visible, activeMenu[index], selected, !unlocked, font)

func _executeChoice(choice:String) -> void:
	if nowPlaying == choice:
		AudioPlayer.stopMusic()
		nowPlaying = ""
		return

	match choice:
		"(Back)":
			nowPlaying = ""
			set_process(false)
			AudioPlayer.fadeMusic(1)
			AudioPlayer.playSound(Sounds.sweepUp)
			yield(ScreenTransitioner.transitionOut(1.0, ScreenTransitioner.DIAMONDS), "completed")
			yield(get_tree().create_timer(0.5), "timeout")
			var mainScene:PackedScene = load("res://scenes/menu-scene.tscn")
			SceneManager.changeScene(mainScene)
			AudioPlayer.playSound(Sounds.sweepDown)
			yield(ScreenTransitioner.transitionIn(1.0, ScreenTransitioner.DIAMONDS), "completed")
		"Title":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/title.ogg"))
		"Level 1":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/level1.ogg"))
		"Level 2":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/level2.ogg"))
		"Level 3":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/metal-remix.ogg"))
		"Boss":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/boss.ogg"))
		"Credits":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/credits.ogg"))
		"Intro":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/intro.ogg"))
		"Unlock":
			nowPlaying = choice
			AudioPlayer.playMusic(load("res://music/unlock.ogg"))
