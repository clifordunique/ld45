extends Sprite

const sound:AudioStream = preload("res://sfx/confirm.wav")

export var feature:String

func _ready():
	if Unlocks.isUnlocked(feature):
		queue_free()
		return
	$Area2D.connect("body_entered", self, "_on_Area2D_body_entered")

func _on_Area2D_body_entered(body:PhysicsBody2D):
	var player:Player = body as Player
	if player != null:
		AudioPlayer.playSound(sound)
		Unlocks.unlock(feature)
		queue_free()